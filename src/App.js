
import React from 'react';
import './App.css';
import SearchImages from './components/SearchImages';
import { Container, Row, Col } from 'reactstrap';


function App() {
  return (
    // <div className="App">
      <Container>
        <Row>
          <Col>
            <h1 className="title">Unsplash </h1>
            <SearchImages /> 
          </Col>
        </Row>
      </Container>
    // </div>
  );
}
export default App;
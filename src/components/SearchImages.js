import React, { useState, useHistory } from "react";
import Unsplash, { toJson } from "unsplash-js";

import 'photoswipe/dist/photoswipe.css';
import 'photoswipe/dist/default-skin/default-skin.css';
import { Gallery, Item } from 'react-photoswipe-gallery';
import "../App";
import { Container, Row, Col } from 'reactstrap';

const unsplash = new Unsplash({
  accessKey: "pCTjuGppmCmrI-TiHHUhtWnoOeb9Rxl2Jxn7N4exC-Y",
});


function SearchImages() {

  const [query, setQuery] = useState("");
  const [pics, setPics] = useState([]);


  const searchPhotos = async (e) => {
    e.preventDefault();

    unsplash.search
      .photos(query)
      .then(toJson)
      .then((json) => {
        // console.log(json);
        setPics(json.results);
      });

  };

  const onChange = () => {
    setPics(pics = pics || [])
  }

  const handleKeyPress = (target) => {
    if(target.charCode==13){
      console.log('Enter clicked, change page');
    }
  }


  return (
    <>
      <Container>
        <Row>
          <Col>
            <form className="form" onSubmit={searchPhotos}>
              <input
                type="text"
                name="query"
                className="input"
                placeholder="Write something ..."
                value={query}
                onChange={(e) => setQuery(e.target.value)}
                onKeyPress={handleKeyPress}
              />
            </form>

            <div className="gallery">
              <Gallery>
                {pics.map((pic) => (
                  <Item
                    key={pic.id}
                    className="imgItem"
                    original={pic.urls.full}
                    thumbnail={pic.urls.small}
                    width="1024"
                    height="768"
                    title={'Name: ' + pic.user.username + `<br />` + 'Location: ' + pic.user.location}
                  >
                      {({ ref, open }) => (
                        <img ref={ref} onClick={open} src={pic.urls.small} />
                      )}
                  </Item>
                ))}{" "}
              </Gallery>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default SearchImages;
